package com.laroy.sophian.handwriting.handwriting.Adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.TextView;
import com.laroy.sophian.handwriting.handwriting.Activities.SelectHandwritingActivity;
import com.laroy.sophian.handwriting.handwriting.Models.Handwriting;
import com.laroy.sophian.handwriting.handwriting.R;

import java.util.ArrayList;

/**
 * Adapter for the grid view of all the handwritings available
 */
public class GridViewHandwritingAdapter extends BaseAdapter {

    private static final String TAG = "GridHandwritingAdapter";

    private SelectHandwritingActivity mContext;

    private ArrayList<Handwriting> listHandwriting;

    public GridViewHandwritingAdapter(SelectHandwritingActivity selectHandwritingActivity) {
        mContext = selectHandwritingActivity;
        listHandwriting = new ArrayList<Handwriting>(mContext.getListHandwriting());
    }

    @Override
    public int getCount() {
        return listHandwriting.size();
    }

    @Override
    public Handwriting getItem(int i) {
        return listHandwriting.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        if (convertView == null) {
            // If convertView is null then inflate the appropriate layout file
            convertView = LayoutInflater.from(mContext).inflate(R.layout.item_select_handwriting, null);
        }

        TextView textViewSelectHandwriting = (TextView) convertView.findViewById(R.id.textview_select_handwriting);
        textViewSelectHandwriting.setText(listHandwriting.get(position).getTitle());

        return convertView;
    }
}

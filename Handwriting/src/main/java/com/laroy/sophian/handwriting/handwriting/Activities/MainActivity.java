package com.laroy.sophian.handwriting.handwriting.Activities;

import android.app.Activity;
import android.content.Intent;
import android.graphics.*;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.laroy.sophian.handwriting.handwriting.App;
import com.laroy.sophian.handwriting.handwriting.Dialogs.ColorPickerDialog;
import com.laroy.sophian.handwriting.handwriting.Interfaces.IPopulateMapRenderPNG;
import com.laroy.sophian.handwriting.handwriting.Models.APIErrors;
import com.laroy.sophian.handwriting.handwriting.Utils.HandwritingUtils;
import com.laroy.sophian.handwriting.handwriting.Interfaces.IHandwritingService;
import com.laroy.sophian.handwriting.handwriting.Models.Handwriting;
import com.laroy.sophian.handwriting.handwriting.R;

import me.angrybyte.numberpicker.listener.OnValueChangeListener;
import me.angrybyte.numberpicker.view.ActualNumberPicker;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;


/**
 * The main activity where the user can convert his text
 * @author Sophian Laroy
 */
public class MainActivity extends AppCompatActivity implements IPopulateMapRenderPNG {

    private static final String TAG = "MainActivity";

    //tokens for connect to ENDPOINT
    public static final String API_TOKEN  = "NR5WEBX3R8JTH9W8";
    public static final String API_SECRET = "M4CYBNDHN659A17M";


    private Callback<List<Handwriting>> callBackGetHandwritings;
    private Callback<Handwriting> callBackGetHandwritingById;
    private Callback<Response> callBackgetRenderPNG;
    private Callback<Response> callBackGetRenderHandwriting;

    private ImageView imageRender;
    private EditText editTextForRender;
    private Button buttonConvert;
    private Button buttonOptions;
    private View layoutOptions;
    private ImageView imageTextColor;
    private ActualNumberPicker actualNumberPicker;
    private ImageView imageHandwriting;

    private int windowWidth = 0;
    private int windowHeight = 0;
    private HashMap<String, String> paramsForRender;
    private IHandwritingService handwritingService = null;
    private ArrayList<Handwriting> listHandwritings;

    private static final int DEFAULT_TEXT_SIZE = 60;
    private static final String DEFAULT_TEXT_COLOR = ColorPickerDialog.intColorToString(ContextCompat.getColor(App.getContext(), R.color.black));

    private static final float LINE_SPACING_VARIANCE_RANDOMIZE = 0.15f;
    private static final float WORD_SPACING_VARIANCE_RANDOMIZE = 0.15f;

    private int textSize;
    private String textColor;
    private boolean randomizeText;
    private String textToRender;
    private final int requestCodeSelectedHandwritingActivity = 90;
    private Handwriting currentHandwriting = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textSize = DEFAULT_TEXT_SIZE;
        textColor = DEFAULT_TEXT_COLOR;
        randomizeText = false;

        paramsForRender = new HashMap<String, String>();

        initViews();
        createCallbacks();

        handwritingService = HandwritingUtils.getHandwritingService(API_TOKEN, API_SECRET);

        if (handwritingService != null) {
            handwritingService.getHandwritings(callBackGetHandwritings);
        }

        hideLoading();
    }

    private void getMainLayoutDimensions() {
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        View v = findViewById(R.id.layout_main_activity);
        windowWidth = v.getWidth();
        windowHeight = v.getHeight();
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        getMainLayoutDimensions();
        setWidthRender(windowWidth);
    }

    /**
     * Initialize all the views of the content
     */
    private void initViews() {

        imageRender = (ImageView) findViewById(R.id.image_render);

        editTextForRender = (EditText) findViewById(R.id.edit_text_for_render);
        buttonConvert = (Button) findViewById(R.id.button_convert);
        buttonConvert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                renderToPNG(paramsForRender);
            }
        });

        layoutOptions = findViewById(R.id.layout_options);

        buttonOptions = (Button) findViewById(R.id.button_options);
        buttonOptions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (layoutOptions.getVisibility() == View.GONE || layoutOptions.getVisibility() == View.INVISIBLE) {
                    setOptionsVisible();
                } else {
                    setOptionsInvisible();
                }
            }
        });

        View layoutOptionsColorPicker = findViewById(R.id.include_options_color_picker);
        if (layoutOptionsColorPicker != null) {
            layoutOptionsColorPicker.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    (new ColorPickerDialog(MainActivity.this, textColor)).show();
                }
            });
        }

        imageTextColor = (ImageView) findViewById(R.id.image_color_text);
        if (imageTextColor != null) {
            imageTextColor.setBackgroundColor(Color.parseColor(DEFAULT_TEXT_COLOR));
        }

        CheckBox chechBoxRandomizeText = (CheckBox) findViewById(R.id.checkBox_randomize_text);
        if (chechBoxRandomizeText != null) {
            chechBoxRandomizeText.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    randomizeText = b;
                }
            });
        }

        ActualNumberPicker actualNumberPicker = (ActualNumberPicker) findViewById(R.id.actual_picker);
        if (actualNumberPicker != null) {
            actualNumberPicker.setListener(new OnValueChangeListener() {
                @Override
                public void onValueChanged(int oldValue, int newValue) {
                    textSize = newValue;
                }
            });
        }

        imageHandwriting = (ImageView) findViewById(R.id.image_handwriting);

        View layoutOptionsHandwriting = findViewById(R.id.include_options_handwriting);
        if (layoutOptionsHandwriting != null) {
            layoutOptionsHandwriting.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startSelectHandwritingActivity();
                }
            });
        }
    }

    /**
     * Set visible the options and change text button options
     */
    private void setOptionsVisible() {
        layoutOptions.setVisibility(View.VISIBLE);
        buttonOptions.setText(R.string.less_options);
    }

    /**
     * Set invisible the options and change text button options
     */
    private void setOptionsInvisible() {
        layoutOptions.setVisibility(View.GONE);
        buttonOptions.setText(R.string.more_options);
    }

    /**
     * Create all the callbacks
     */
    private void createCallbacks() {

        //Callback for getting all Handwritings
        callBackGetHandwritings = new Callback<List<Handwriting>>() {
            @Override
            public void success(List<Handwriting> handwritings, Response response) {
                listHandwritings = new ArrayList<Handwriting>(handwritings);
                currentHandwriting = listHandwritings.get(0);
                setHandwritingIdRender(currentHandwriting.getId()); //first handwriting = Molly
                setImageHandwriting();
            }

            @Override
            public void failure(RetrofitError error) {
                listHandwritings = new ArrayList<Handwriting>();
                if (error.getMessage().equals("401 Unauthorized")) {
                    displayTextWithSnackBar(R.string.wrong_login_or_pwd);
                } else {
                    displayTextWithSnackBar(R.string.error_generic);
                }
                buttonConvert.setEnabled(true);
                hideLoading();
            }
        };

        //Callback for getting a Handwriting with an id
        callBackGetHandwritingById = new Callback<Handwriting>() {
            @Override
            public void success(Handwriting handwriting, Response response) {
            }

            @Override
            public void failure(RetrofitError error) {
                if (error.getMessage().equals("401 Unauthorized")) {
                    displayTextWithSnackBar(R.string.wrong_login_or_pwd);
                } else {
                    displayTextWithSnackBar(R.string.error_generic);
                }
                buttonConvert.setEnabled(true);
                hideLoading();
            }

        };

        //Callback for getting a rendering image
        //If success, this callback is setting an imageview with the binary data receive
        callBackgetRenderPNG = new Callback<Response>() {
            @Override
            public void success(Response response, Response response2) {
                TypedByteArray raw = (TypedByteArray) response.getBody();
                setByteArrayToImageView(raw, imageRender);
                buttonConvert.setEnabled(true);
                hideLoading();
            }

            @Override
            public void failure(RetrofitError error) {
                APIErrors body = (APIErrors) error.getBodyAs(APIErrors.class);
                displayTextWithSnackBar(body.getStringError());
                buttonConvert.setEnabled(true);
                hideLoading();
            }
        };

        callBackGetRenderHandwriting = new Callback<Response>() {
            @Override
            public void success(Response response, Response response2) {
                TypedByteArray raw = (TypedByteArray) response.getBody();
                setByteArrayToImageView(raw, imageHandwriting);
            }

            @Override
            public void failure(RetrofitError error) {
                APIErrors body = (APIErrors) error.getBodyAs(APIErrors.class);
                displayTextWithSnackBar(body.getStringError());
            }
        };
    }

    /**
     * Set the image view with the image render of the current handwriting
     */
    private void setImageHandwriting() {
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("handwriting_id", currentHandwriting.getId());
        params.put("text", currentHandwriting.getTitle());
        params.put("handwriting_size", 80+"px");
        params.put("height", "auto");
        renderToPNGHandwriting(params);
    }

    /**
     * Method to get a Handwriting by it's id
     * This method calls the service
     * @param handwritingId the id of the Handwriting wanted
     */
    private void getHandwritingById(String handwritingId) {
        if (HandwritingUtils.isNetworkAvailable(MainActivity.this)) {
            handwritingService.getHandwritingById(handwritingId, callBackGetHandwritingById);
        } else {
            displayTextWithSnackBar(R.string.no_network);
        }
    }


    /**
     * Method to get a image render PNG with the parameters in the HashMap paramsForRender
     * This method calls the service
     * @param params the HashMap witch contains all the parameters for the rendering
     */
    private void renderToPNG(HashMap params) {
        buttonConvert.setEnabled(false);
        displayLoading();
        if (editTextForRender.getText().toString().isEmpty()) {
            editTextForRender.setError("Please write a text");
            buttonConvert.setEnabled(true);
            hideLoading();
        } else {
            editTextForRender.setError(null);
            textToRender = editTextForRender.getText().toString();
            setAllParamsForRender();

            if (HandwritingUtils.isNetworkAvailable(MainActivity.this)) {
                if (!params.containsKey("handwriting_id") || !params.containsKey("text")) {
                    displayTextWithSnackBar(R.string.missing_parameters_render);
                    buttonConvert.setEnabled(true);
                    hideLoading();
                } else {
                    handwritingService.getRenderPNG(params, callBackgetRenderPNG);
                }
            } else {
                displayTextWithSnackBar(R.string.no_network);
                buttonConvert.setEnabled(true);
                hideLoading();
            }
        }


    }

    /**
     * Method to get a image render PNG of the handwriting
     * This method calls the service
     * @param params the HashMap witch contains all the parameters for the rendering
     */
    private void renderToPNGHandwriting(HashMap<String, String> params) {
        if (HandwritingUtils.isNetworkAvailable(MainActivity.this)) {
            if (!params.containsKey("handwriting_id") || !params.containsKey("text")) {
                displayTextWithSnackBar(R.string.missing_parameters_render);
            } else {
                handwritingService.getRenderPNG(params, callBackGetRenderHandwriting);
            }
        } else {
            displayTextWithSnackBar(R.string.no_network);
        }
    }

    /**
     * Hide the layout to inform the user that the system is loading
     */
    private void hideLoading() {
        findViewById(R.id.layout_convert_loading).setVisibility(View.INVISIBLE);
    }

    /**
     * Display the layout to inform the user that the system is loading
     */
    private void displayLoading() {
        findViewById(R.id.layout_convert_loading).setVisibility(View.VISIBLE);

    }

    /**
     * Display a text with a snackbar
     * @param text the string text to display
     */
    private void displayTextWithSnackBar(String text) {
        Snackbar.make(findViewById(R.id.layout_main_activity), text, Snackbar.LENGTH_LONG).show();
    }

    /**
     * Display a text with a snackbar
     * @param id the id of the string ressource to display
     */
    private void displayTextWithSnackBar(int id) {
        Snackbar.make(findViewById(R.id.layout_main_activity), id, Snackbar.LENGTH_LONG).show();
    }

    /**
     * This methods get a ByteArray, decode it to a Bitmap and then put it in an ImageView
     * @param raw the ByteArray to put in an ImageView
     * @param image the ImageView to fill with raw
     */
    private void setByteArrayToImageView(TypedByteArray raw, ImageView image) {
        Bitmap bm = BitmapFactory.decodeByteArray(raw.getBytes(), 0, raw.getBytes().length);
        image.setImageBitmap(bm);
    }

    /**
     * Method calls just before ask the API to render a text
     * This method fill the hash map of parameters with the wanted values
     */
    private void setAllParamsForRender() {
        setTextRender(textToRender);
        setHandwritingSizeRender(textSize);
        setHandwritingColorRender(textColor);
        setRandomizeRender(randomizeText);
    }


    @Override
    public void setHandwritingIdRender(String handwritingId) {
        paramsForRender.put("handwriting_id", handwritingId);
    }

    @Override
    public void setTextRender(String textToRender) {
        paramsForRender.put("text", textToRender);
    }

    @Override
    public void setHandwritingColorRender(String color) {
        paramsForRender.put("handwriting_color", color);
    }

    @Override
    public void setHeightRender(String height) {
        if (height.equals("auto")) {
            paramsForRender.put("height", height);
        } else {
            displayTextWithSnackBar(R.string.wrong_height);
        }
    }

    @Override
    public void setHeightRender(int height) {
        paramsForRender.put("height", height+"px");
    }


    @Override
    public void setWidthRender(int width) {
        paramsForRender.put("width", width+"px");
    }

    @Override
    public void setHandwritingSizeRender(int handwritingSize) {
        paramsForRender.put("handwriting_size", handwritingSize+"px");
    }

    @Override
    public void setRandomizeRender(boolean randomizeText) {
        float lineSpacingVariance;
        float wordSpacingVariance;
        int randomSeed;
        if (randomizeText) {
            lineSpacingVariance = LINE_SPACING_VARIANCE_RANDOMIZE;
            wordSpacingVariance = WORD_SPACING_VARIANCE_RANDOMIZE;
            randomSeed = -1;
        } else {
            lineSpacingVariance = 0;
            wordSpacingVariance = 0;
            randomSeed = 1;
        }
        paramsForRender.put("line_spacing_variance", ""+lineSpacingVariance);
        paramsForRender.put("word_spacing_variance", ""+wordSpacingVariance);
        paramsForRender.put("random_seed", ""+randomSeed);
    }

    /**
     * Override dispatchTouchEvent
     * Solution to clearFocus and hideKeyboard when touch is outside an edittext
     * Answer by zMan : http://stackoverflow.com/questions/4828636/edittext-clear-focus-on-touch-outside
     */
    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if ( v instanceof EditText) {
                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int)event.getRawX(), (int)event.getRawY())) {
                    v.clearFocus();
                    hideKeyboard();
                }
            }
        }
        return super.dispatchTouchEvent( event );
    }

    /**
     * Hide the keyboard and clear the focus on editTextForRender
     */
    private void hideKeyboardAndClearFocusEditText() {
        editTextForRender.clearFocus();
        hideKeyboard();
    }

    /**
     * Hide the keyboard
     */
    private void hideKeyboard() {
        InputMethodManager inputMethodManager =(InputMethodManager)getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(findViewById(R.id.layout_main_activity).getWindowToken(), 0);
    }

    /**
     * Method to set the text color for the render, it also changes the background color of the ImageView Text Color
     * @param textColor the new text color
     */
    public void setTextColor(String textColor) {
        this.textColor = textColor;
        this.imageTextColor.setBackgroundColor(Color.parseColor(textColor));
    }

    /**
     * Start SelectHandwritingActivity
     */
    private void startSelectHandwritingActivity() {
        Intent i = new Intent(MainActivity.this, SelectHandwritingActivity.class);
        i.putParcelableArrayListExtra(SelectHandwritingActivity.LIST_HANDWRITING, listHandwritings);
        startActivityForResult(i, requestCodeSelectedHandwritingActivity);
    }

    /**
     * Called when the second activity's finished
     * */
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch(requestCode) {
            case requestCodeSelectedHandwritingActivity:
                if (resultCode == RESULT_OK) {
                    Bundle res = data.getExtras();
                    int newHandwritingPosition = res.getInt(SelectHandwritingActivity.NEW_HANDWRITING_POSITION);
                    if (newHandwritingPosition != -1) {
                        currentHandwriting = listHandwritings.get(newHandwritingPosition);
                        setHandwritingIdRender(currentHandwriting.getId());
                        setImageHandwriting();
                    }
                }
                break;
        }
    }
}

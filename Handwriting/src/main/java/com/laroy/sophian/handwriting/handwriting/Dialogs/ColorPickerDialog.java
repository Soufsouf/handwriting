package com.laroy.sophian.handwriting.handwriting.Dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import com.laroy.sophian.handwriting.handwriting.Activities.MainActivity;
import com.laroy.sophian.handwriting.handwriting.R;
import com.larswerkman.holocolorpicker.ColorPicker;
import com.larswerkman.holocolorpicker.SVBar;

/**
 * Custom Dialog for choose a color for the text, via a color picker
 */
public class ColorPickerDialog extends Dialog {

    private static final String TAG = "ColorPickerDialog";

    private ColorPicker picker;
    private Button buttonOk;
    private Button buttonCancel;

    public ColorPickerDialog(Context context, String textColor) {
        super(context);
        this.setContentView(R.layout.dialog_color_picker);
        this.setTitle(R.string.choose_color);
        this.setCanceledOnTouchOutside(true);

        if (context instanceof Activity) {
            setOwnerActivity((Activity) context);
        }

        initViews(textColor);
    }

    /**
     * Initialisation of all views
     * @param textColor the textColor of the activity
     */
    private void initViews(String textColor) {
        picker = (ColorPicker) findViewById(R.id.color_picker);
        SVBar svBar = (SVBar) findViewById(R.id.sv_bar);

        picker.addSVBar(svBar);

        if (picker != null) {
            picker.setShowOldCenterColor(false);
            picker.setColor(Color.parseColor(textColor));
        }

        buttonOk = (Button) findViewById(R.id.button_ok_color_picker);
        buttonOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity) getOwnerActivity()).setTextColor(intColorToString(picker.getColor()));
                ColorPickerDialog.this.dismiss();
            }
        });

        buttonCancel = (Button) findViewById(R.id.button_cancel_color_picker);
        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ColorPickerDialog.this.cancel();
            }
        });

    }

    /**
     * Convert a color int to a string hex color
     * @param i the int value of the color
     */
    public static String intColorToString(int i) {
        return String.format("#%06X", (0xFFFFFF & i));
    }
}

package com.laroy.sophian.handwriting.handwriting.Models;

import com.google.gson.annotations.SerializedName;
import com.laroy.sophian.handwriting.handwriting.App;
import com.laroy.sophian.handwriting.handwriting.R;

import java.util.List;

/**
 * Class to handle the API Errors when there is a Bad Request
 */
public class APIErrors {

    @SerializedName("errors")
    private List<APIError> errors;

    public String getStringError() {
        if (errors.get(0).error.equals("text contains characters that do not exist in the selected handwriting")) { //error for calls render PNG and PDF
            return App.getContext().getString(R.string.error_characters);
        } else if (errors.get(0).error.equals("rate limit exceeded")) { //error for all calls to the API
            return App.getContext().getString(R.string.error_rate_limit);
        } else if (errors.get(0).error.equals("not found")) { //error for calls handwriting by id
            return App.getContext().getString(R.string.error_handwriting_not_found);
        } else {
            return App.getContext().getString(R.string.error_generic);
        }
    }

    @Override
    public String toString() {
        return errors.get(0).error;
    }

    private class APIError {
        @SerializedName("error")
        public String error;
    }
}

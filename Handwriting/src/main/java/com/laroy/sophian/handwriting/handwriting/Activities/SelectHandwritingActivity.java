package com.laroy.sophian.handwriting.handwriting.Activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.*;
import com.laroy.sophian.handwriting.handwriting.Adapters.GridViewHandwritingAdapter;
import com.laroy.sophian.handwriting.handwriting.Interfaces.IHandwritingService;
import com.laroy.sophian.handwriting.handwriting.Models.APIErrors;
import com.laroy.sophian.handwriting.handwriting.Models.Handwriting;
import com.laroy.sophian.handwriting.handwriting.R;
import com.laroy.sophian.handwriting.handwriting.Utils.HandwritingUtils;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Activity when the user can select a new handwriting
 */
public class SelectHandwritingActivity extends AppCompatActivity {

    private static final String TAG = "SelectHandwritingAct";

    public static final String LIST_HANDWRITING = "listHandwriting";
    public static final String NEW_HANDWRITING_POSITION = "new_handwriting_position";

    private ArrayList<Handwriting> listHandwriting;

    private Callback<Response> callBackGetRenderHandwriting;

    private Toolbar toolbar;
    private GridView gridViewHandwriting;
    private ImageView imageExempleHandwriting;
    private TextView textViewExempleHandwriting;

    private IHandwritingService handwritingService = null;
    private int handwritingPositionsSelected = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_handwriting);

        Intent intent = getIntent();
        listHandwriting = intent.getParcelableArrayListExtra(LIST_HANDWRITING);

        handwritingService = HandwritingUtils.getHandwritingService(MainActivity.API_TOKEN, MainActivity.API_SECRET);

        callBackGetRenderHandwriting = new Callback<Response>() {
            @Override
            public void success(Response response, Response response2) {
                TypedByteArray raw = (TypedByteArray) response.getBody();
                setByteArrayToImageView(raw, imageExempleHandwriting);
                imageExempleHandwriting.setVisibility(View.VISIBLE);
                hideLoading();
            }

            @Override
            public void failure(RetrofitError error) {
                APIErrors body = (APIErrors) error.getBodyAs(APIErrors.class);
                displayTextWithSnackBar(body.getStringError());
                hideLoading();
            }
        };

        initViews();
    }

    /**
     * Initialisation of the views
     */
    private void initViews() {

        hideLoading();

        toolbar = (Toolbar) findViewById(R.id.tool_bar_select_handwriting);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        gridViewHandwriting = (GridView) findViewById(R.id.grid_view_handwritings);
        gridViewHandwriting.setAdapter(new GridViewHandwritingAdapter(this));

        if (gridViewHandwriting != null) {
            gridViewHandwriting.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                    setImageHandwriting(listHandwriting.get(position));
                    textViewExempleHandwriting.setVisibility(View.INVISIBLE);
                    imageExempleHandwriting.setVisibility(View.INVISIBLE);
                    displayLoading();
                    handwritingPositionsSelected = position;
                }
            });
        }

        textViewExempleHandwriting = (TextView) findViewById(R.id.textview_exemple_handwriting);
        imageExempleHandwriting = (ImageView) findViewById(R.id.image_exemple_handwriting);

        Button buttonValidate = (Button) findViewById(R.id.button_validate_handwriting);
        if (buttonValidate != null) {
            buttonValidate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    finishWithResult();
                }
            });
        }
    }

    /**
     * Finish and sending a result code with a intent to get the new handwriting selected by the user
     */
    private void finishWithResult() {
        Bundle conData = new Bundle();
        conData.putInt(NEW_HANDWRITING_POSITION, handwritingPositionsSelected);
        Intent intent = new Intent();
        intent.putExtras(conData);
        setResult(RESULT_OK, intent);
        finish();
    }

    /**
     * Set the image view with the image render of the handwriting
     * @param handwriting the handwriting to display
     */
    private void setImageHandwriting(Handwriting handwriting) {
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("handwriting_id", handwriting.getId());
        params.put("text", handwriting.getTitle()+ " : " + getString(R.string.exemple_for_handwriting));
        params.put("handwriting_size", 80+"px");
        params.put("height", "auto");
        params.put("width", imageExempleHandwriting.getWidth()+"px");
        params.put("random_seed", "1");
        renderToPNGHandwriting(params);
    }

    /**
     * Method to get a image render PNG of the handwriting
     * This method calls the service
     * @param params the HashMap witch contains all the parameters for the rendering
     */
    private void renderToPNGHandwriting(HashMap<String, String> params) {
        if (HandwritingUtils.isNetworkAvailable(SelectHandwritingActivity.this)) {
            if (!params.containsKey("handwriting_id") || !params.containsKey("text")) {
                displayTextWithSnackBar(R.string.missing_parameters_render);
            } else {
                handwritingService.getRenderPNG(params, callBackGetRenderHandwriting);
            }
        } else {
            displayTextWithSnackBar(R.string.no_network);
        }
    }

    /**
     * Display a text with a snackbar
     * @param text the string text to display
     */
    private void displayTextWithSnackBar(String text) {
        Snackbar.make(findViewById(R.id.layout_main_activity), text, Snackbar.LENGTH_LONG).show();
    }

    /**
     * Display a text with a snackbar
     * @param id the id of the string ressource to display
     */
    private void displayTextWithSnackBar(int id) {
        Snackbar.make(findViewById(R.id.layout_main_activity), id, Snackbar.LENGTH_LONG).show();
    }

    /**
     * This methods get a ByteArray, decode it to a Bitmap and then put it in an ImageView
     * @param raw the ByteArray to put in an ImageView
     * @param image the ImageView to fill with raw
     */
    private void setByteArrayToImageView(TypedByteArray raw, ImageView image) {
        Bitmap bm = BitmapFactory.decodeByteArray(raw.getBytes(), 0, raw.getBytes().length);
        image.setImageBitmap(bm);
    }

    public ArrayList<Handwriting> getListHandwriting() {
        return listHandwriting;
    }

    /**
     * Hide the layout to inform the user that the system is loading
     */
    private void hideLoading() {
        findViewById(R.id.layout_exemple_loading).setVisibility(View.INVISIBLE);
    }

    /**
     * Display the layout to inform the user that the system is loading
     */
    private void displayLoading() {
        findViewById(R.id.layout_exemple_loading).setVisibility(View.VISIBLE);

    }

}

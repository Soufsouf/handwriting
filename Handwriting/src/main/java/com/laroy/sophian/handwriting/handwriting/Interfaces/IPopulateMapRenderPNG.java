package com.laroy.sophian.handwriting.handwriting.Interfaces;

import com.laroy.sophian.handwriting.handwriting.R;

import java.util.HashMap;


/**
 * Interface with all the methods to populate the parameters for rendering a PNG Image
 */
public interface IPopulateMapRenderPNG {

    /**
     * Add a handwritingId parameter in a HashMap for the render.
     * This parameter is MANDATORY to get a render image
     * @param handwritingId the Handwriting id to display the text in this font in the image rendered
     */
    public void setHandwritingIdRender(String handwritingId);

    /**
     * Add a text parameter in a HashMap for the render.
     * This parameter is MANDATORY to get a render image
     * @param textToRender the text to display in the image rendered
     */
    public void setTextRender(String textToRender);

    /**
     * Add a color parameter in a HashMap for the render.
     * This parameter is not mandatory to get a render image
     * @param color the text color, ex : #451678
     */
    public void setHandwritingColorRender(String color);

    /**
     * Add a width parameter in a HashMap for the render.
     * This parameter is not mandatory to get a render image
     * @param width the text width in pixels
     */
    public void setWidthRender(int width);

    /**
     * Add a height parameter in a HashMap for the render.
     * This parameter is not mandatory to get a render image
     * @param height the text height string : must be "auto"
     */
    public void setHeightRender(String height);

    /**
     * Add a height parameter in a HashMap for the render.
     * This parameter is not mandatory to get a render image
     * @param height the text height in pixels
     */
    public void setHeightRender(int height);

    /**
     *
     * Add a width parameter in a HashMap for the render.
     * This parameter is not mandatory to get a render image
     * @param handwritingSize the text size in pixels
     */
    public void setHandwritingSizeRender(int handwritingSize);


    /**
     * Add a severals parameters in a HashMap for a different render every time.
     * This parameter is not mandatory to get a render image
     * @param randomizeText If true, the text is randomize, if false, the text is not randomize
     */
    void setRandomizeRender(boolean randomizeText);
}

package com.laroy.sophian.handwriting.handwriting.Models;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

/**
 * Model for the class Handwriting
 * It describe a handwriting font
 * This font is provided by https://handwriting.io/
 * @author Sophian Laroy
 */
public class Handwriting implements Parcelable {

    @SerializedName("id")
    private String id;

    @SerializedName("title")
    private String title;

    @SerializedName("date_created")
    private Date dateCreated;

    @SerializedName("date_modified")
    private Date dateModified;

    @SerializedName("rating_neatness")
    private int ratingNeatness;

    @SerializedName("rating_cursivity")
    private int ratingCursivity;

    @SerializedName("rating_embellishment")
    private int ratingEmbellishment;

    @SerializedName("rating_character_width")
    private int ratingCharacterWidth;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Date getDateModified() {
        return dateModified;
    }

    public void setDateModified(Date dateModified) {
        this.dateModified = dateModified;
    }

    public int getRatingNeatness() {
        return ratingNeatness;
    }

    public void setRatingNeatness(int ratingNeatness) {
        this.ratingNeatness = ratingNeatness;
    }

    public int getRatingCursivity() {
        return ratingCursivity;
    }

    public void setRatingCursivity(int ratingCursivity) {
        this.ratingCursivity = ratingCursivity;
    }

    public int getRatingEmbellishment() {
        return ratingEmbellishment;
    }

    public void setRatingEmbellishment(int ratingEmbellishment) {
        this.ratingEmbellishment = ratingEmbellishment;
    }

    public int getRatingCharacterWidth() {
        return ratingCharacterWidth;
    }

    public void setRatingCharacterWidth(int ratingCharacterWidth) {
        this.ratingCharacterWidth = ratingCharacterWidth;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int i) {
        dest.writeString(id);
        dest.writeString(title);
        dest.writeLong(dateCreated.getTime());
        dest.writeLong(dateModified.getTime());
        dest.writeInt(ratingNeatness);
        dest.writeInt(ratingCursivity);
        dest.writeInt(ratingEmbellishment);
        dest.writeInt(ratingCharacterWidth);
    }

    public Handwriting(Parcel source) {
        id = source.readString();
        title = source.readString();
        dateCreated = new Date(source.readLong());
        dateModified = new Date(source.readLong());
        ratingNeatness = source.readInt();
        ratingCursivity = source.readInt();
        ratingEmbellishment = source.readInt();
        ratingCharacterWidth = source.readInt();
    }

    public static final Parcelable.Creator<Handwriting> CREATOR = new Parcelable.Creator<Handwriting>() {
        @Override
        public Handwriting[] newArray(int size) {
            return new Handwriting[size];
        }

        @Override
        public Handwriting createFromParcel(Parcel source) {
            return new Handwriting(source);
        }
    };

    @Override
    public String toString() {
        String res = "";
        res += id + " ; ";
        res += title + " ; ";;
        res += dateCreated.toString()+ " ; ";
        res += dateModified.toString()+ " ; ";
        return res;
    }
}

package com.laroy.sophian.handwriting.handwriting.Utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Base64;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.laroy.sophian.handwriting.handwriting.Interfaces.IHandwritingService;
import com.squareup.okhttp.OkHttpClient;

import java.util.concurrent.TimeUnit;

import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.android.AndroidLog;
import retrofit.client.OkClient;
import retrofit.converter.GsonConverter;

/**
 * Util class for the functionnalities access of the API https://api.handwriting.io
 * @author Sophian Laroy
 */
public class HandwritingUtils {

    private static final String ENDPOINT = "https://api.handwriting.io";
    private static int TIMEOUT_VALUE = 10;

    private static IHandwritingService sHandwritingServiceInstance = null;

    /**
     * Check if there is an internet connection
     * @param context the context
     * @return true if there is an internet connection, false otherwise
     */
    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.
                getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        // if no network is available networkInfo will be null
        // otherwise check if we are connected
        if (networkInfo != null && networkInfo.isConnected()) {
            return true;
        }
        return false;
    }

    /**
     * Set the HandwritingService for communicate with the API https://api.handwriting.io
     * @param username the login to connect to the API https://api.handwriting.io
     * @param password the password to connect to the API https://api.handwriting.io
     * @return the instance sHandwritingServiceInstance or create it
     */
    public static IHandwritingService getHandwritingService(String username, String password) {

        if (sHandwritingServiceInstance == null) {
            if (username != null && password != null) {
                // concatenate username and password with colon for authentication
                String credentials = username + ":" + password;
                // create Base64 encodet string
                final String basic =
                        "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);

                final OkHttpClient okHttpClient = new OkHttpClient();
                okHttpClient.setReadTimeout(TIMEOUT_VALUE, TimeUnit.SECONDS);
                okHttpClient.setConnectTimeout(TIMEOUT_VALUE, TimeUnit.SECONDS);

                final RestAdapter.Builder builder = new RestAdapter.Builder()
                        .setEndpoint(ENDPOINT)
                        .setLog(new AndroidLog("retrofit"))
                        .setConverter(new LenientGsonConverter(getGsonBuilder()))
                        .setLogLevel(RestAdapter.LogLevel.FULL)
                        .setClient(new OkClient(okHttpClient));

                builder.setRequestInterceptor(new RequestInterceptor() {
                    @Override
                    public void intercept(RequestFacade request) {
                        request.addHeader("Authorization", basic);
                        request.addHeader("Accept", "application/json");
                    }
                });


                sHandwritingServiceInstance = builder
                        .build()
                        .create(IHandwritingService.class);

                return sHandwritingServiceInstance;

            } else {
                return null;
            }
        } else {
            return sHandwritingServiceInstance;
        }
    }

    /**
     * @return a Gson for the converter
     */
    private static Gson getGsonBuilder() {
        return new GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'hh:mm:ss.SSS'Z'")
                .create();
    }


}

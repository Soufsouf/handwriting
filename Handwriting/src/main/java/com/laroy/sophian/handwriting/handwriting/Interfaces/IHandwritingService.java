package com.laroy.sophian.handwriting.handwriting.Interfaces;

import com.laroy.sophian.handwriting.handwriting.Models.Handwriting;

import retrofit.Callback;
import retrofit.client.Response;
import retrofit.http.*;

import java.util.List;
import java.util.Map;

/**
 * Retrofit interface for the communication with the API handwriting.io
 * @author Sophian Laroy
 */
public interface IHandwritingService {

        /**
         * Method to get the list of all handwritings
         * @param callback the callback to handle the retrofit response
         */
        @GET("/handwritings")
        void getHandwritings(Callback<List<Handwriting>> callback);

        /**
         * Method to get a Handwriting
         * @param id the id of the Handwriting wanted
         * @param callback the callback to handle the retrofit response
         */
        @GET("/handwritings/{id}")
        void getHandwritingById(@Path("id")  String id, Callback<Handwriting> callback);

        /**
         * Method to get a Image Render PNG
         * @param params all the parameters to generate the PNG Image
         * @param callback the callback to handle the retrofit response
         */
        @GET("/render/png")
        void getRenderPNG(@QueryMap Map<String, String> params, Callback<Response> callback);

}

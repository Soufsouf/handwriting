package com.laroy.sophian.handwriting.handwriting;

import android.app.Application;
import android.content.Context;

/**
 * Override Application class to access a context everywhere
 */
public class App extends Application {

    private static Context sContext;

    @Override
    public void onCreate() {
        super.onCreate();
        sContext = this;
    }

    public static Context getContext(){
        return sContext;
    }
}

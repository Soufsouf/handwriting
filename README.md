Handwriting project's README :


Handwriting is an Android application developped by Sophian Laroy for the test of the entreprise PayMyTable.
It's developped via IntelliJ (Android Studio).

The purpose of this app is to provide to the user an handwriting text from whatever text he wants.
For generate the handwriting text, we use the API Handwriting.io : https://handwriting.io/.

To render the handwriting text, the user has to enter the text he wants to convert to handwriting then he clicked on "Convert" to get an image of the handwriting text.
This app needs an internet connection.

This app use Retrofit librairy to access to the API Handwriting.io via the methods in IHandwritingService.
For the choose the color of the text, I selected ColorPicker from https://github.com/LarsWerkman/HoloColorPicker. I chose it because it was rated good in Android Arsenal and can provide all the rgb colors.
I also choose Actual Number Picker from https://github.com/milosmns/actual-number-picker. It was just what I wanted, a simple, small integer picker.


V1.0 :
Tape a text in the edittext zone and press convert : you will get a image with your text convert in a handwriting font.

V2.0 :
The user can now specify a size and a color the his text.
Also, he can generate every time a different image, slicy different every render.
Finally, he can choose between all the handwritings of https://handwriting.io/



